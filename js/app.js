function main() {
    var formGeocentricsUTM = new FormGeocentricsUTM();
}

// Class for the formulaty of Geocentrics to UTM

var FormGeocentricsUTM = function() {
    this.initElements();
    this.initOnKeyUpEvent();
    this.initButtonCalculateClickEvent();
};

FormGeocentricsUTM.prototype.initElements = function() {
    this.inputPositionX = document.querySelector('#input-position-x');
    this.inputPositionY = document.querySelector('#input-position-y');
    this.inputPositionZ = document.querySelector('#input-position-z');
    this.inputVelocityX = document.querySelector('#input-velocity-x');
    this.inputVelocityY = document.querySelector('#input-velocity-y');
    this.inputVelocityZ = document.querySelector('#input-velocity-z');
    this.inputCurrentYear =document.querySelector('#input-current-year');
    this.buttonCalculate = document.querySelector('#button-calculate');
}

FormGeocentricsUTM.prototype.initOnKeyUpEvent = function() {
    var that = this;
    this.inputPositionX.onkeyup = this.inputPositionY.onkeyup =
        this.inputPositionZ.onkeyup = this.inputVelocityX.onkeyup =
        this.inputVelocityY.onkeyup = this.inputVelocityZ.onkeyup =
        this.inputCurrentYear.onkeyup = function() {
	    if(that.inputPositionX.value == "" ||
	       that.inputPositionY.value == "" ||
	       that.inputPositionZ.value == "" ||
	       that.inputVelocityX.value == "" ||
	       that.inputVelocityY.value == "" ||
	       that.inputVelocityZ.value == "" ||
	       that.inputCurrentYear.value == "")
		that.buttonCalculate.disabled = true;
	    else
		that.buttonCalculate.disabled = false;
	}
}

FormGeocentricsUTM.prototype.initButtonCalculateClickEvent = function() {
    var that = this;
    this.buttonCalculate.onclick = function() {
	if(that.inputPositionX.value == 0)
	    utils.status.show(
		document.webL10n.get("x-position-cant-be-zero"), 4000);
    }
}

init_sidebar_buttons_events();

function init_sidebar_buttons_events() {
    // Button help
    document.querySelector('#button-help-section').
        addEventListener('click', function () {
            show_section('#section-help');
        });
    document.querySelector('#button-help-section-back').
        addEventListener ('click', function () {
            back_section('#section-help');
        });
}

function show_section(sectionName) {
    document.querySelector(sectionName).className = 'current';
    document.querySelector('[data-position="current"]')
        .className = 'left';
}

function back_section(sectionName) {
    document.querySelector('[data-position="current"]')
        .className = 'current';
    document.querySelector(sectionName).className = 'right';
}

/* Sidebar buttons */
document.querySelector('#button-geocentrics-utm').
    addEventListener('click', function () {
        document.querySelector('#form-geocentrics-utm').className = '';
        document.querySelector('#form-geographics-utm')
            .className = 'hidden';
    });

document.querySelector('#button-geographics-utm').
    addEventListener('click', function () {
        document.querySelector('#form-geocentrics-utm')
            .className = 'hidden';
        document.querySelector('#form-geographics-utm').className = '';
    });

/* Open the results */
// document.querySelector('#button-calculate').
//     addEventListener('click', function () {
//         document.querySelector('#results-section').className = 'current';
//         document.querySelector('[data-position="current"]')
//             .className = 'left';
//     });
// document.querySelector('#button-results-section-back').
//     addEventListener ('click', function () {
//         document.querySelector('[data-position="current"]')
//             .className = 'current';
//         document.querySelector('#results-section').className = 'right';
//     });


main();
